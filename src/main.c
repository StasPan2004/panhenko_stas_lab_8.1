#define _CRT_SECURE_NO_WARNINGS // для возможности использования scanf
#include <stdio.h> //библиотека для использования printf & scanf
#include "main.h"

int main() /// Точка входу у программу.
{
	root(); /// Запуск функціі для розрахунку квадратного кореня.
	matrix(); /// Запуск функціі для помноження матриці.
	return 0;
}
double root() ///  Функція для підрахунку квадратного кореня числа
{
	/** Для того, щоб розразувати корінь, нам потрібно повторювати  розрахунок x2
	 за допомогою формули Герона допоки i не буде менше b.
 */

	double a, x = 1, x2, b = 0.000001, i = 0; // объявляем переменные
	printf("Enter a  positive number: \a");
	scanf("%lf",
	      &a); // ввод числа с которого будет высчитыватся квадратный корень
		//начало цикла
	do {
		x2 = (x + a / x) / 2; //Итерационная формула Герона

		i = x2 - x;
		if (i < 0) //условие для корректной  работы цикла
			i = -i;
		x = x2;
	} while (i >= b);
	printf("The root of number = %lf \n", x2); //вывод результата
	return x2;
}
///Перед початком подальших розрахунків оголошуємо двовимірні масиви
int A[3][3] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

int B[3][3] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
void outvec(int n, int *vec)
{
	for (int i = 0; i < n; i++)
		printf("%d ", vec[i]);
	printf("\n");
}
int martixmain() /// Функція для множення мариць
{
	int C[3][3] = { 0 };
	int i, j, k;
	int mA = 3;
	int nA = 3;
	int mB = 3;
	int nB = 3;
	//C = A*B
	for (i = 0; i < mA; i++)
		for (j = 0; j < nB; j++)
			for (k = 0; k < nA; k++) {
				C[i][j] += A[i][k] * B[k][j];
			}
	printf("Matrix C:\n");
	for (i = 0; i < mA; i++)
		outvec(nB, C[i]);
	return 0;
}

int matrix()
{
	martixmain();
	return 0;
}
